const fetch = require('node-fetch');
const fs = require('fs');

const mutualFundHouseTypeCount = require('./mutualFundHouseTypeCount');
const mutualFundHouseCount = require('./mutualFundHouseCount');
const topTenMF = require('./topTenMF');
const averageYearOne = require('./averageYearOne');

fetch('https://api.kuvera.in/api/v3/funds.json')
    .then(res => res.json())
    .then(jsonData => {

        const fetchedData = jsonData;

        //fs.writeFile('./data/fetchedData.json', JSON.stringify(fetchedData), function (err) { if (err) { console.log(err); } });

        const resultMutualFundHouseCount = JSON.stringify(mutualFundHouseCount(fetchedData));
        fs.writeFile('./output/mutualFundHouseCount.json', resultMutualFundHouseCount, function (err) { if (err) { console.log(err); } })

        const resultMutualFundHouseTypeCount = JSON.stringify(mutualFundHouseTypeCount(fetchedData));
        fs.writeFile('./output/mutualFundHouseTypeCount.json', resultMutualFundHouseTypeCount, function (err) { if (err) { console.log(err); } })

        const resultTopTenMF = JSON.stringify(topTenMF(fetchedData));
        fs.writeFile('./output/topTenMF.json', resultTopTenMF, function (err) { if (err) { console.log(err); } })

        const resultAverageYearOne = JSON.stringify(averageYearOne(fetchedData));
        fs.writeFile('./output/averageYearOne.json', resultAverageYearOne, function (err) { if (err) { console.log(err); } })

    }).catch(err => console.error(err));

//number of mf for each fund house

function mutualFundHouseCount(fetchedData) {

    return fetchedData.reduce((accumulator, data) => {

        const fundHouse = data.fund_house;

        if (fundHouse in accumulator) { accumulator[fundHouse] += 1; }
        else { accumulator[fundHouse] = 1; }

        return accumulator;
    }, {});
}

module.exports = mutualFundHouseCount;

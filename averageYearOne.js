//average year_1 return rate for each fund house

function averageYearOne(fetchedData) {

    const yearOneFundHouseObject = fetchedData.reduce((accumulator, data) => {

        const fundHouse = data.fund_house;
        const yearOne = data.returns.year_1;

        if (fundHouse in accumulator) { if (yearOne !== undefined) { accumulator[fundHouse].push(data.returns.year_1); } }
        else { accumulator[fundHouse] = []; }

        return accumulator;
    }, {});

    const averageYearOneObject = {};
    for (key in yearOneFundHouseObject) { averageYearOneObject[key] = (yearOneFundHouseObject[key].reduce((first, second) => first + second, 0) / yearOneFundHouseObject[key].length).toFixed(3); }

    return averageYearOneObject;
}

module.exports = averageYearOne;

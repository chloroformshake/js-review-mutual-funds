//number of mutual funds of each fund_type for each fund_house. 

function mutualFundHouseTypeCount(fetchedData) {

    return fetchedData.reduce((accumulator, data) => {

        const fundHouse = data.fund_house;
        const fundType = data.fund_type;

        if (fundHouse in accumulator) {

            if (fundType in accumulator[fundHouse] && fundType !== undefined) { accumulator[fundHouse][fundType] += 1; }
            else { if (fundType !== undefined) { accumulator[fundHouse][fundType] = 1; } }
        }
        else { accumulator[fundHouse] = {}; }

        return accumulator;
    }, {});
}

module.exports = mutualFundHouseTypeCount;

//top 10 mutual funds based on year_5 returns

function topTenMF(fetchedData) {
    return fetchedData.sort((first, second) => { return second.returns.year_5 - first.returns.year_5; }).slice(0, 10);
}

module.exports = topTenMF;
